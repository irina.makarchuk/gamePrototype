package weapon;

import lombok.Getter;

@Getter
public class Bow implements Weapon {
    private String weapon = "bow";

    @Override
    public String getWeapon() {
        return weapon;
    }
    @Override
    public String toString() {
        return weapon;
    }
}
