package weapon;

import lombok.Getter;

@Getter
public class Sword implements Weapon {
    private String weapon = "sword";

    @Override
    public String getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return weapon;
    }
}
