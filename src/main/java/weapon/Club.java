package weapon;

import lombok.Getter;

@Getter
public class Club implements Weapon {
    private String weapon = "club";

    @Override
    public String getWeapon() {
        return weapon;
    }
    @Override
    public String toString() {
        return weapon;
    }
}
