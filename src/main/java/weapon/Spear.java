package weapon;

import lombok.Getter;

@Getter
public class Spear implements Weapon {
    private String weapon = "spear";

    @Override
    public String getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return weapon;
    }
}
