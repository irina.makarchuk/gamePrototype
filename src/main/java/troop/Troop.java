package troop;

import races.Race;

import java.util.List;

public interface Troop {
    PlayerNumberGenerator number = new PlayerNumberGenerator();
    LifeController controller = new LifeController();
    Group group = new Group();

    String setName(Race race); //giving each player the name according to his race;
    String getName();
    void setCurrentGroup(String currentGroup);
    String getCurrentGroup(); //obtaining the group the player belongs to;
    int getCurrentNumber(); //obtaining the number of the player;
    void setCurrentLife(double currentLife); //setting the life level after changing it;
    double getCurrentLife(); //obtaining the actual life level;

    default String attack(List<Troop> rivalTeam, double loss, Troop rival) {
        String complete =  rival.getName() + " (#" + rival.getCurrentNumber() + ")" + " has got reducing the power by " + String.format("%.0f",loss);
        return group.actForOrdinaryOrPrivileged(rivalTeam, rival, loss, complete);
    }

    default String attack(List<Troop> rivalTeam, String weapon, double loss, Troop rival) {
        String complete = rival.getName() + " (#" + rival.getCurrentNumber() + ")" + " has been attacked by the " + weapon + ". The power is reduced by " + String.format("%.0f",loss);
        return group.actForOrdinaryOrPrivileged(rivalTeam, rival, loss, complete);
    }

    default String improve(Troop player) {
        String completeForOrdinary = player.getName() + " (#" + player.getCurrentNumber() + ") has been moved to the Privileged group.";
        String completeForPrivileged = player.getName() + " (#" + player.getCurrentNumber() + ") is already Privileged. One can't get Privileged twice.";
        return group.actForOrdinaryOrPrivileged(player, completeForOrdinary, completeForPrivileged);
    }

    default String reduce(List<Troop> rivalTeam, Troop rival, double loss) {
        String complete = rival.getName() + " (#" + rival.getCurrentNumber() + ")" + " has got the magic attack. The power is reduced by " + String.format("%.0f",loss);
        return group.actForOrdinaryOrPrivileged(rivalTeam, rival, loss, complete);
    }
}