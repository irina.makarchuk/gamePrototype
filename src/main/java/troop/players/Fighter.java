package troop.players;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import races.Orcs;
import races.Race;
import races.Undeads;
import troop.Troop;

@ToString
@Getter
@Setter
@Slf4j
public class Fighter implements Troop {
    private String name = "Fighter";
    private int currentNumber;
    private String currentGroup;
    private double currentLife;

    public String getName() {
        return name;
    }

    public Fighter() {
        currentNumber = number.getNextNumber();
        currentGroup = group.getOrdinaryStatus();
        currentLife = controller.getCurrentLife();
    }

    @Override
    public String setName(Race race) {
        if (race instanceof Orcs) {
            return this.name = "Goblin";
        } else if (race instanceof Undeads) {
            return this.name = "Zombie";
        } else {
            return name;
        }
    }
}