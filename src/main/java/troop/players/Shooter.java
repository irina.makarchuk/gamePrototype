package troop.players;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import races.*;
import troop.Troop;

import java.util.List;

@ToString
@Getter
@Setter
public class Shooter implements Troop {
    private String name = "Shooter";
    private int currentNumber;
    private String currentGroup;
    private double currentLife;

    public Shooter() {
        currentNumber = number.getNextNumber();
        currentGroup = group.getOrdinaryStatus();
        currentLife = controller.getCurrentLife();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String setName(Race race) {
        if ((race instanceof Orcs) || (race instanceof Elves)) {
            return this.name = "Archer";
        } else if (race instanceof People) {
            return this.name = "Arbalester";
        } else if (race instanceof Undeads) {
            return this.name = "Hunter";
        } else {
            return name;
        }
    }

    public String shoot(List<Troop> rivalTeam, String weapon, double loss, Troop rival) {
        String complete = rival.getName() + " (#" + rival.getCurrentNumber() + ")" + " has been shot by the " + weapon + ". The power is reduced by " + String.format("%.0f",loss);
        return group.actForOrdinaryOrPrivileged(rivalTeam, rival, loss, complete);
    }
}