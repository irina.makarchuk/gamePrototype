import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;

@Slf4j
class Greeter {
    private Launcher launcher = new Launcher();

    void greet() {
        System.out.println("=======================================");
        System.out.println("===                                 ===");
        System.out.println("===            WELCOME              ===");
        System.out.println("===                                 ===");
        System.out.println("=======================================");
        System.out.println();
        System.out.println("=== Press \"YES\" to start the game.  ===");
        System.out.println("=== Press \"NO\" to exit.             ===");
        System.out.println();
        input();
    }

    private void input() {
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        if (answer.equalsIgnoreCase("yes")) {
            System.out.println("Let's get started.\n");
            log.info("Starting the game...");
            launcher.createRaces();
            launcher.startExecution();
        } else if(answer.equalsIgnoreCase("no")) {
            log.info("The player has exited.");
            System.out.println("You've exited.");
        } else {
            System.out.println("=== You should enter \"YES\" or \"NO\". ===");
            input();
        }
    }
}
