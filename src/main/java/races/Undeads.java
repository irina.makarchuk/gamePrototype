package races;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Undeads implements Race {
    private String race = "Undeads";

    @Override
    public String toString() {
        return race;
    }
}