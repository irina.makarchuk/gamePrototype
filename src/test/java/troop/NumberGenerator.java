package troop;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class NumberGenerator {
    private PlayerNumberGenerator generator = new PlayerNumberGenerator();

    @Test
    public void shouldReturnOne() throws Exception {
        assertEquals(1, generator.getNextNumber());
    }

    @Test
    public void shouldReturnOneAndTwo() throws Exception {
        assertEquals(1, generator.getNextNumber());
        assertEquals(2, generator.getNextNumber());
    }

    @Test
    public void shouldNotReturnNine() throws Exception {
        assertEquals(1, generator.getNextNumber());
        assertEquals(2, generator.getNextNumber());
        assertEquals(3, generator.getNextNumber());
        assertEquals(4, generator.getNextNumber());
        assertEquals(5, generator.getNextNumber());
        assertEquals(6, generator.getNextNumber());
        assertEquals(7, generator.getNextNumber());
        assertEquals(8, generator.getNextNumber());
        assertNotEquals(9, generator.getNextNumber());
    }

    @Test
    public void shouldReturnCurrentOne() throws Exception {
        assertEquals(1, generator.getCurrentNumber());
    }
}