package troop;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;

public class Controller {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    private LifeController controller;
    private LifeController mockController;
    private Troop mockPlayer;

    @Before
    public void setUpMock() {
        controller = new LifeController();
        mockController = mock(LifeController.class);
        mockPlayer = mock(Troop.class);
    }

    @Test
    public void changeLifeSize() throws Exception {
        doCallRealMethod().when(mockController).changeLifeSize(anyList(), eq(mockPlayer), anyInt());
    }

    @Test
    public void changeLifeSize1() throws Exception {
        doCallRealMethod().when(mockController).changeLifeSize(anyList(), eq(mockPlayer));
    }

    @Test
    public void shouldShowIfThePlayerIsAlive() throws Exception {
        doCallRealMethod().when(mockController).isAlive(mockPlayer);
    }

    @Test
    public void shouldReturnTextKilled() throws Exception {
        assertEquals(" The player has been killed.", controller.killed());
    }

}